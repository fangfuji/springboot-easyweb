//package com.guoj.easyweb.filter;
//
//import com.guoj.easyweb.common.CurrentUser;
//import com.guoj.easyweb.entity.TbUser;
//import lombok.extern.slf4j.Slf4j;
//
//import javax.servlet.*;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
///**
// * @作者: guoj
// * @日期: 2019/6/21 18:11
// * @描述: 登录验证过滤器
// */
//@Slf4j
//public class LoginFilter implements Filter {
//	@Override
//	public void init(FilterConfig filterConfig) throws ServletException {
//
//	}
//
//	@Override
//	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
//			throws IOException, ServletException {
//		HttpServletRequest req = (HttpServletRequest) servletRequest;
//		HttpServletResponse resp = (HttpServletResponse) servletResponse;
//		System.out.println("===============进入登录过滤器==============");
//		System.out.println(req.getRequestURL());
//
//		TbUser user = (TbUser) req.getSession().getAttribute("user");
//		System.out.println("user=" + user);
//		if (user == null) {
//			resp.sendRedirect("/spa/login.html");
//			return;
//		}
//
//		// 存在则说明已登录，存入threadLocal。
////		CurrentUser.add(user);
////		System.out.println("currentUser=" + CurrentUser.getCurrentUser());
//
//		// 放行
//		filterChain.doFilter(servletRequest, servletResponse);
//	}
//
//	@Override
//	public void destroy() {
//
//	}
//}
