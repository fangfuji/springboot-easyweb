package com.guoj.easyweb.listener;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @作者: guoj
 * @日期: 2019/6/22 10:15
 * @描述: session监听器
 */
@WebListener
@Slf4j
public class SessionListener implements HttpSessionListener {

	// 使用AtomicInteger 来统计在线人数
	private AtomicInteger onLineCount = new AtomicInteger(0);

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		log.info("创建Session....");
//		event.getSession().getServletContext().setAttribute("onLineCount", onLineCount.incrementAndGet());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		log.info("销毁Session....");
//		event.getSession().getServletContext().setAttribute("onLineCount", onLineCount.decrementAndGet());
	}

}
