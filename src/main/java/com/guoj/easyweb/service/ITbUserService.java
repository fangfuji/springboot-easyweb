package com.guoj.easyweb.service;

import com.guoj.easyweb.entity.TbUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.guoj.easyweb.form.AppLoginForm;
import com.guoj.easyweb.form.LoginForm;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author guoj
 * @since 2019-06-20
 */
public interface ITbUserService extends IService<TbUser> {
	/**
	 * 登录
	 * @param loginForm
	 * @return
	 */
	TbUser login(LoginForm loginForm);

	/**
	 * App端登录
	 * @param loginForm
	 * @return
	 */
	TbUser appLogin(AppLoginForm loginForm);

	/**
	 * 添加用户
	 * @param user
	 * @return
	 */
	int addUser(TbUser user);

}
