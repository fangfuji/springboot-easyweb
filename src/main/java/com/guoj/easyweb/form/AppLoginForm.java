package com.guoj.easyweb.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @作者: guoj
 * @日期: 2019/6/20 18:06
 * @描述: App登录验证
 */
@Data
public class AppLoginForm {

	@NotBlank(message = "用户名不能为空")
	private String username;

	@NotBlank(message = "密码不能为空")
	private String password;

}
