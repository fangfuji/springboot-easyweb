package com.guoj.easyweb.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @作者: guoj
 * @日期: 2019/6/20 18:06
 * @描述:
 */
@Data
public class LoginForm {

	@NotBlank(message = "用户名不能为空")
	private String username;

	@NotBlank(message = "密码不能为空")
	private String password;

	@NotBlank(message = "验证码不能为空")
	private String captcha;
}
