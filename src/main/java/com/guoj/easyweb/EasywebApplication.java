package com.guoj.easyweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;

/**
 * 使用 @ServletComponentScan注解后，Servlet、Filter、Listener
 * 可以直接通过 @WebServlet、@WebFilter、@WebListener 注解自动注册，无需其他代码
 */
@SpringBootApplication
@ServletComponentScan
public class EasywebApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasywebApplication.class, args);
	}

}
