package com.guoj.easyweb.mapper;

import com.guoj.easyweb.entity.TbUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author guoj
 * @since 2019-06-20
 */
public interface TbUserMapper extends BaseMapper<TbUser> {

}
