package com.guoj.easyweb.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author guoj
 * @since 2019-06-20
 */
public class TbUser extends Model<TbUser> {

private static final long serialVersionUID=1L;

    /**
     * 主键
     */
	// 注解处理Long类型前后端传递精度缺失问题，这里可以配置公共 baseEntity 处理
	@JsonSerialize(using=ToStringSerializer.class)
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 加盐
     */
    private String salt;

    /**
     * 启用禁用，0-禁用，1-启用。
     */
    private Integer flag;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 性别，0-女，1-男。
     */
    private Integer sex;

    /**
     * 住址
     */
    private String address;

    /**
     * 头像地址
     */
    private String headerImg;

	/**
	 * 第三方联合登录授权后返回的openId
	 */
	private String openId;

	/**
	 * 创建时间
	 */
	private LocalDateTime createTime;

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHeaderImg() {
        return headerImg;
    }

    public void setHeaderImg(String headerImg) {
        this.headerImg = headerImg;
    }

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public LocalDateTime getCreateTime() {
		return createTime;
	}

	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbUser{" +
        "id=" + id +
        ", username=" + username +
        ", password=" + password +
        ", salt=" + salt +
        ", flag=" + flag +
        ", nickname=" + nickname +
        ", sex=" + sex +
        ", address=" + address +
        ", headerImg=" + headerImg +
        ", openId=" + openId +
        ", createTime=" + createTime +
        "}";
    }

}
