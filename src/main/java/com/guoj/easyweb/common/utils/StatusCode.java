package com.guoj.easyweb.common.utils;

/**
 * @作者: guoj
 * @日期: 2019/4/30 16:10
 * @描述: 状态码
 */
public class StatusCode {
	public static final int OK=200;// 成功
	public static final int LAYUI_OK=0;// layui规定返回成功的状态码code=0
	public static final int ERROR=20001;// 失败
	public static final int LOGINERROR=20002;// 用户名或密码错误
	public static final int ACCESSERROR=20003;// 权限不足
	public static final int REMOTEERROR=20004;// 远程调用失败
	public static final int REPERROR=20005;// 重复操作
	public static final int DUPLICATEKEYERROR=20006;// 数据库中已存在该记录
	public static final int USERLOCKED=20007;// 账号被锁定
	public static final int CAPTCHAERROR=20008;// 验证码有误
	public static final int NOHANDLERFOUNDERROR=404;// 路径不存在
	public static final int SC_INTERNAL_SERVER_ERROR=500;// 系统逻辑异常
	public static final int SESSION_EXPIRE=408;// session超时
}
