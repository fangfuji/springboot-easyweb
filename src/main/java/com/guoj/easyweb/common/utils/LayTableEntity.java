package com.guoj.easyweb.common.utils;

/**
 * @作者: guoj
 * @日期: 2019/4/30 16:10
 * @描述: 封装LayUI.table返回数据格式
 * 			-- 根据layui.table规范的响应格式
 * 			-- 格式如下：
 * 			{
				"code": 0,
				"msg": "",
				"count": 1000,
				"data": [
						{
							"id": 10000,
							"username": "user-0",
							"sex": "女",
							"city": "城市-0"
						},
						{
							"id": 10001,
							"username": "user-1",
							"sex": "男",
							"city": "城市-1"
						}
					]
				}
 */
public class LayTableEntity {

	private Integer code;

	private String msg;

	private Long count;

	private Object data;

	public LayTableEntity() {
	}

	public LayTableEntity(Integer code, String msg, Long count) {
		this.code = code;
		this.msg = msg;
		this.count = count;
	}

	public LayTableEntity(Integer code, String msg, Long count, Object data) {
		this.code = code;
		this.msg = msg;
		this.count = count;
		this.data = data;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
