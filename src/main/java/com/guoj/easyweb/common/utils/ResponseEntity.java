package com.guoj.easyweb.common.utils;

/**
 * @作者: guoj
 * @日期: 2019/4/30 16:10
 * @描述: 封装业务接口返回数据
 */
public class ResponseEntity {

	private boolean flag;

	private Integer code;

	private String message;

	private Object data;

	public ResponseEntity() {
	}

	public ResponseEntity(boolean flag, Integer code, String message) {
		this.flag = flag;
		this.code = code;
		this.message = message;
	}

	public ResponseEntity(boolean flag, Integer code, String message, Object data) {
		this.flag = flag;
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
