package com.guoj.easyweb.common.utils;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;

/**
 * @作者: guoj
 * @日期: 2019/6/24 00:46
 * @描述: 加密工具类
 * 			-- Random生成的盐 + sha256Hex混合加密
 */
public class EncryptUtil {

	public static final String DEFAULT_PASSWORD = "123456";// 默认密码

	/**
	 * 加密
	 * @param password
	 * @return 返回数组，包含加密后字符串和盐值
	 */
	public static String[] encrypt(String password) {
		// 传过来值为空，就用默认密码加密。
		if (StringUtils.isBlank(password)) {
			password = DEFAULT_PASSWORD;
		}

		// 随机生成盐，保存在数据库用户信息中。
		String salt = RandomStringUtils.randomAlphanumeric(20);
		// 加盐加密
		String encryptPass = DigestUtils.sha256Hex(password + salt);
		String[] result = new String[]{encryptPass, salt};
		return result;
	}
}
