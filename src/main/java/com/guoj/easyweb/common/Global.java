package com.guoj.easyweb.common;

/**
 * @作者: guoj
 * @日期: 2019/6/21 22:07
 * @描述:
 */
public class Global {

	/**
	 * 配置MVC跳转地址
	 */
	//============================================================================================================//
	public static final String WELCOME_PAGE = "view/welcome";
	public static final String USER_LIST_PAGE = "view/sys/userListPage";
	//============================================================================================================//
}
