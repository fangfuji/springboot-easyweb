package com.guoj.easyweb.common;

import com.guoj.easyweb.entity.TbUser;
import javax.servlet.http.HttpServletRequest;

/**
 * ThreadLocal工具类
 */
public class RequestHolder {

    private static final ThreadLocal<TbUser> userHolder = new ThreadLocal<TbUser>();

    private static final ThreadLocal<HttpServletRequest> requestHolder = new ThreadLocal<HttpServletRequest>();

    public static void add(TbUser user) {
        userHolder.set(user);
    }

    public static void add(HttpServletRequest request) {
        requestHolder.set(request);
    }

    public static TbUser getCurrentUser() {
        return userHolder.get();
    }

    public static HttpServletRequest getCurrentRequest() {
        return requestHolder.get();
    }

    public static void remove() {
        userHolder.remove();
        requestHolder.remove();
    }
}
