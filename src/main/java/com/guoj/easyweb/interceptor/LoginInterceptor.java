package com.guoj.easyweb.interceptor;


import com.guoj.easyweb.entity.TbUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录拦截器
 */
@Component
@Slf4j
public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		log.info("登录拦截器....{}", request.getRequestURL());

		TbUser user = (TbUser) request.getSession().getAttribute("user");
		if (user == null) {
			log.error("未登录，进行拦截....");
			response.sendRedirect("/spa/login.html");
			return false;
		}

		log.info("session有效期是：{}s", request.getSession().getMaxInactiveInterval());

        return true;
    }
}
