package com.guoj.easyweb.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @作者: guoj
 * @日期: 2019/6/27 10:42
 * @描述: swagger-bootstrap-ui
 * 			-- swagger的增强UI配置类
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {
	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.guoj.easyweb.app.controller")) //扫描该路径下的api文档
				.paths(PathSelectors.any()) //路径判断
				.build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("Easyweb 在线API文档") //标题
				.description("swagger-bootstrap-ui swagger的增强工具 官方地址--->>>>：https://gitee.com/xiaoym/swagger-bootstrap-ui") //描述
				.termsOfServiceUrl("http://localhost:8999/") //（不可见）条款地址
				.contact("2609745657@qq.com")
				.version("1.0") //版本号
				.build();
	}
}
