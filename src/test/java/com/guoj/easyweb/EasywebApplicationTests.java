package com.guoj.easyweb;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guoj.easyweb.common.utils.IDUtil;
import com.guoj.easyweb.entity.TbUser;
import com.guoj.easyweb.mapper.TbUserMapper;
import com.guoj.easyweb.service.ITbUserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EasywebApplicationTests {

	@Autowired
	private ITbUserService tbUserService;

	@Test
	public void contextLoads() {
	}

	/**
	 * 验证加解密
	 */
	@Test
	public void testSHA256() {
		String password = "123";
		// 随机生成盐，保存在数据库用户信息中。
		String salt = RandomStringUtils.randomAlphanumeric(20);
		System.out.println("================= 盐 ================");
		System.out.println(salt);
		System.out.println("================= 盐 ================");
		// 加盐加密
		String result = DigestUtils.sha256Hex(password + salt);
		System.out.println("================= 加盐加密后 ================");
		System.out.println(result);
		System.out.println("================= 加盐加密后 ================");
	}

	/**
	 * 创建用户
	 */
	@Test
	public void testCreateUser() throws InterruptedException {
//		// 创建一个admin用户
//		TbUser user = new TbUser();
//		user.setId(IDUtil.getId(1));
//		user.setUsername("admin");
//		user.setNickname("心梦无痕");
//		user.setFlag(1);
//		user.setSex(1);
//		user.setPassword("61f4c962d2f9cc63fda4e9e6674648655a2067bd4b29b434eb2abc38bb022b3d");
//		user.setAddress("广州市天河区车陂路大塘中街");
//		user.setSalt("bdHCyfXHm2oRThJEhfuX");
//		user.setHeaderImg("http://m.360buyimg.com/pop/jfs/t23434/230/1763906670/10667/55866a07/5b697898N78cd1466.jpg");
//		user.setCreateTime(LocalDateTime.now());
//		tbUserService.save(user);

		for (int i=0;i<105;i++){
			TbUser user = new TbUser();
			user.setId(IDUtil.getId(1));
			user.setUsername("guojing" + (i==0?"":i));
			user.setNickname("郭靖" + (i==0?"":i));
			user.setFlag(1);
			user.setSex(1);
			user.setPassword("61f4c962d2f9cc63fda4e9e6674648655a2067bd4b29b434eb2abc38bb022b3d");
			user.setAddress("湖北省襄阳市襄城区内环路18号");
			user.setSalt("bdHCyfXHm2oRThJEhfuX");
			user.setHeaderImg("http://m.360buyimg.com/pop/jfs/t23434/230/1763906670/10667/55866a07/5b697898N78cd1466.jpg");
			user.setCreateTime(LocalDateTime.now());
			tbUserService.save(user);

			Thread.sleep(1000);
		}

	}


}
