# springboot-easyweb

#### 介绍
springboot整合easyweb的spa版，实现了easyweb-spa版本中用户管理模块的功能，其他模块都可以参照此模块来进行快速开发。

#### 软件架构
| 技术                 | 版本          |
| -------------------- | ------------- |
| springboot           | 2.1.6.RELEASE |
| easyweb              | v3.1.2        |
| mybatisplus          | 3.0.7.1       |
| mysql                | 5.1.38        |
| druid                | 1.1.13        |
| jjwt                 | 0.7.0         |
| swagger              | 2.9.2         |
| swagger-bootstrap-ui | 1.9.4         |
| fastjson             | 1.2.47        |
| hutool               | 4.1.21        |
| lombok               | 1.18.4        |
| justAuth             | 1.7.1         |
| fastdfs              | 1.27.0.0      |



